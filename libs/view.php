<?php
class View{
	function render($view, $data = null){

		$header = VIEWS . "common/header.php";
		$sidebar = VIEWS . "common/sidebar.php";
		$footer = VIEWS . "common/footer.php";

		$top = FW . VIEWS . "common/top.php";
		$bottom = FW . VIEWS . "common/bottom.php";

		require_once $top;

		if(file_exists($header))
			require_once $header;

		if(file_exists($sidebar))
			require_once $sidebar;
		
		if($view == 'welcome'):
			$app_welcome = VIEWS . "welcome.php";
			$cms_welcome = FW . VIEWS . "welcome.php";
			if(file_exists($app_welcome)):
				require_once $app_welcome;
			else:
				require_once $cms_welcome;
			endif;
		else:
			require_once VIEWS . $view . '.php';
		endif;

		if(file_exists($footer))
			require_once $footer;
		require_once $bottom;
	}
}
?>